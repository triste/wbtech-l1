package main

import (
	"flag"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	sec := flag.Int("s", 4, "Время работы программы в секундах")
	flag.Parse()
	ch := make(chan int)
	var wg sync.WaitGroup
	wg.Add(1)
	// Читающая горутина
	go func() {
		defer wg.Done()
		for data := range ch {
			fmt.Println(data)
		}
	}()
	timer := time.NewTimer(time.Duration(*sec) * time.Second)
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-timer.C:
			// Прошло N секунд, закрываем канал данных и ждём завершение читающей горутины
			close(ch)
			wg.Wait()
			return
		case <-ticker.C:
			// Каждую секунду генерируются и отправляются данные в канал
			ch <- rand.Int()
		}
	}
}
