package main

import (
	"fmt"
	"log"
	"math/big"
)

func main() {
	var a, b big.Int
	fmt.Print("Введите два числа:")
	if _, err := fmt.Scan(&a, &b); err != nil {
		log.Fatal(err)
	}
	var c big.Int
	fmt.Printf("a * b = %v\n", c.Mul(&a, &b))
	fmt.Printf("a / b = %v\n", c.Quo(&a, &b))
	fmt.Printf("a + b = %v\n", c.Add(&a, &b))
	fmt.Printf("a - b = %v\n", c.Sub(&a, &b))
}
