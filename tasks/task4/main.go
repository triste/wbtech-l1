package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"time"
)

func main() {
	workerCount := flag.Int("w", 8, "Количество воркеров")
	flag.Parse()
	// Канал в который основная горутина пишет, а воркеры читают
	dataCh := make(chan int)
	var wg sync.WaitGroup
	for i := 0; i < *workerCount; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			// Читаем до закрытия канала.
			// Альтернативным решением будет использование отдельного канала
			// для сигнализации завершения, но в данном случае у нас одна
			// пишущая горутина, поэтому проще ей делегировать закрытие канала
			// неявно сигнализирующее о завершение
			for data := range dataCh {
				// Симулируем сложные вычисление
				time.Sleep(1 * time.Second)
				fmt.Println(data)
			}
		}()
	}
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, os.Interrupt)
	for {
		select {
		case sig := <-sigCh:
			fmt.Printf("Сигнал %v получен, закрываем канал и ждём завершения воркеров\n", sig)
			close(dataCh)
			wg.Wait()
			return
		default:
			// Если сигнал не получен, то генерируем данные и отправляем в канал
			dataCh <- rand.Int()
		}
	}
}
