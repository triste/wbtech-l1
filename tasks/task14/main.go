package main

import (
	"fmt"
	"reflect"
)

func main() {
	vals := []any{10, "test", true, make(chan int)}
	for _, val := range vals {
		fmt.Println(val)
		// 1. Reflect
		{
			typ := reflect.TypeOf(val)
			fmt.Printf("\tReflect: %v\n", typ)
		}
		{
			// 2. Type switch
			var typ string
			switch val.(type) {
			case int:
				typ = "int"
			case string:
				typ = "string"
			case bool:
				typ = "bool"
			case chan int:
				typ = "chan int"
			}
			fmt.Printf("\tType switch: %v\n", typ)
		}

		{
			// 3. Type assertions
			var typ string
			if _, ok := val.(int); ok {
				typ = "int"
			} else if _, ok := val.(string); ok {
				typ = "string"
			} else if _, ok := val.(bool); ok {
				typ = "bool"
			} else if _, ok := val.(chan int); ok {
				typ = "chan int"
			}
			fmt.Printf("\tType assertion: %v\n", typ)
		}
	}
}
