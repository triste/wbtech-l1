package main

import "fmt"

type Human struct {
	name    string
	Surname string
	age     int
}

func (h *Human) SetAge(n int) {
	if n > 0 {
		h.age = n
	}
}

func (h *Human) SetName(name string) {
	h.name = name
}

type Action struct {
	Human
	name string
}

func (a *Action) SetName(name string) {
	a.name = fmt.Sprintf("action [%v]", name)
}

func main() {
	human := Human{
		name: "Ivan",
		age:  18,
	}
	action := Action{
		Human: human,
		name:  "Running",
	}
	// Через "дочерний" объект можно напрямую обращаться к полям и методам "родителя"
	action.Surname = "Ivanov"
	action.SetAge(20)
	action.SetName("Walking") // action.name == "action [Walking]"
	// Также можно явно указывать полный путь используя имя встроенной структуры
	// Полезно для доступа к "родительскому" полю/методу в случае если "дочерний"
	// имеет поле/метод с таким же именем
	action.Human.Surname = "Ivanov"
	action.Human.SetAge(20)
	action.Human.SetName("Andrei") // action.Human.name = "Andrei"
}
