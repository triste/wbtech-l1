package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func solveAtomic(nums []int) int {
	// Логика как в task2, за исключением использования атомарной операции
	// добавнения к сумме
	var (
		sum atomic.Int64
		wg  sync.WaitGroup
	)
	for _, num := range nums {
		wg.Add(1)
		go func(num int) {
			defer wg.Done()
			sum.Add(int64(num * num))
		}(num)
	}
	wg.Wait()
	return int(sum.Load())
}

func solveChannel(nums []int) int {
	squares := make(chan int)
	// Запускаем len(nums) горутин пишущих в канал квадратов чисел
	for _, num := range nums {
		go func(num int) {
			squares <- num * num
		}(num)
	}
	var sum int
	// В канал поступит ровно len(nums) квадратов, чтение их всех неявно
	// сигнализирует о том, что все воркер горутины завершили свою работу,
	// т.е нет необходимости в WaitGroup
	for i := 0; i < len(nums); i++ {
		sum += <-squares
	}
	return sum
}

func main() {
	nums := []int{2, 4, 6, 8, 10}
	fmt.Println(solveAtomic(nums))
	fmt.Println(solveChannel(nums))
}
