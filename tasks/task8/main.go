package main

import (
	"fmt"
	"log"
)

func main() {
	fmt.Print("Число, бит и операция(S - установить в единицу, C - очистить, T - переключить): ")
	var (
		num int64
		bit uint
		op  string
	)
	if _, err := fmt.Scan(&num, &bit, &op); err != nil {
		log.Fatal(err)
	}
	if bit > 63 {
		log.Fatalf("бит больше 63")
	}
	mask := int64(1) << bit
	result := num
	switch op {
	case "S":
		if bit == 63 {
			if num > 0 {
				result = -num
			}
		} else {
			result |= mask
		}
	case "C":
		if bit == 63 {
			if num < 0 {
				result = -num
			}
		} else {
			result &= ^mask
		}
	case "T":
		if bit == 63 {
			result = -num
		} else {
			result ^= mask
		}
	default:
		log.Fatalf("неизвестная операция")
	}
	fmt.Printf("%v (%b) -> %v (%b)\n", num, num, result, result)
}
