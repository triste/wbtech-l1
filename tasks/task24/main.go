package main

import (
	"fmt"
	"math"
)

type Point struct {
	x, y float64
}

func NewPoint(x, y float64) *Point {
	return &Point{x, y}
}

func (a *Point) Distance(b *Point) float64 {
	return math.Hypot(a.x-b.x, a.y-b.y)
}

func main() {
	a := NewPoint(10, 13)
	b := NewPoint(4, 8)
	dist := a.Distance(b)
	fmt.Println(dist)
}
