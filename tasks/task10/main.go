package main

import (
	"fmt"
	"sort"
)

func main() {
	temps := []float32{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5}
	m := make(map[int][]float32, len(temps))
	for _, temp := range temps {
		group := int(temp / 10)
		m[group] = append(m[group], temp)
	}
	// мапа неупорядоченная, поэтому берём все ключи и сортируем
	groups := make([]int, 0, len(m))
	for group := range m {
		groups = append(groups, group)
	}
	sort.Ints(groups)
	for _, group := range groups {
		fmt.Println(m[group])
	}
}
