package main

import (
	"fmt"
	"log"
)

func main() {
	nums := []int{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144}
	fmt.Printf("Слайс: %v\n", nums)
	var idx int
	fmt.Print("Индекс удаляемого элемента: ")
	if _, err := fmt.Scan(&idx); err != nil {
		log.Fatal(err)
	}
	if idx < 0 || idx >= len(nums) {
		log.Fatalf("Неверный индекс")
	}
	nums = append(nums[:idx], nums[idx+1:]...)
	fmt.Printf("Результат: %v\n", nums)
}
