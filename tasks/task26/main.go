package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Строка: ")
	line, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}
	line = line[:len(line)-1]
	fmt.Println(containUniqueChars(line))
}

func containUniqueChars(str string) bool {
	set := make(map[rune]struct{}, len(str))
	for _, ch := range str {
		ch := unicode.ToLower(ch)
		if _, found := set[ch]; found {
			return false
		}
		set[ch] = struct{}{}
	}
	return true
}
