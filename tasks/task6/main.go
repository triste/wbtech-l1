package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	// В отличие от тредов, принудительно остановить горутину нельзя,
	// но можно оповестить её о завершение с помощью:
	// 1. Каналов
	// - 1.a. Закрытия read-only канала данных, если такой имеется
	var wg sync.WaitGroup
	dataCh := make(chan int)
	wg.Add(1)
	go func(data <-chan int) {
		defer wg.Done()
		for d := range data {
			fmt.Printf("case 1: %v\n", d)
		}
	}(dataCh)
	// Пишем в канал, закрываем его и ждём завершения горутины
	for i := 0; i < 10; i++ {
		dataCh <- i
	}
	close(dataCh)
	wg.Wait()

	// - 1.b. Отдельного канала или контекста для сигнализации
	sigCh := make(chan struct{})
	wg.Add(1)
	go func(sigCh <-chan struct{}) {
		defer wg.Done()
		for {
			select {
			case <-sigCh:
				return
			default:
				// Если не поступило сигнала, продолжаем выполнять полезную работу
				time.Sleep(time.Second)
				fmt.Println("case 1.b")
			}
		}
	}(sigCh)
	// Спим 4 секунды, оповещаем горутину и ждём её завершения
	time.Sleep(4 * time.Second)
	sigCh <- struct{}{}
	wg.Wait()

	// 2. Разделяемую память
	var done atomic.Bool
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if done.Load() {
				return
			}
			// Если done не равно true, то продолжаем выполнение
			time.Sleep(time.Second)
			fmt.Println("case 2")
		}
	}()
	time.Sleep(4 * time.Second)
	done.Store(true)
	wg.Wait()
}
