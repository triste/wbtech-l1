package main

import "fmt"

func main() {
	a, b := 10, 3
	// 1. Встроенной поддержкой свапа без временной переменной
	a, b = b, a
	fmt.Println(a, b)
	// 2. Математически
	a -= b
	b += a    // b + (a - b) = a
	a = b - a // a - a' = a - (a - b) = b
	fmt.Println(a, b)
}
