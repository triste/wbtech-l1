package main

import (
	"strings"
)

var justString string

func createHugeString(lenght int) string {
	return strings.Repeat("A", lenght)
}

func someFunc() {
	// 1. На хипе создаётся большая строка v и не подчищается сборщиком мусора
	// до тех пор, пока глобальная переменная justString указывает на ту же
	// область памяти что и v
	// 2. Строка может содержать unicode, а индексация побайтовая
	v := createHugeString(1 << 10)
	justString = v[:100]
}

func someFuncFixed() {
	v := createHugeString(1 << 10)
	runes := make([]rune, 100)
	copy(runes, []rune(v)[:100])
	justString = string(runes)
}

func main() {
	someFuncFixed()
	someFunc()
}
