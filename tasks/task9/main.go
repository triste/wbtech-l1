package main

import "fmt"

func main() {
	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9}
	ch1 := make(chan int)
	ch2 := make(chan int)
	go func() {
		for _, num := range nums {
			ch1 <- num
		}
		// закрываем канал, неявно сигнализируя о завершение работы горутины
		close(ch1)
	}()
	go func() {
		// читаем до тех пор, пока канал ch1 не закрыт
		for num := range ch1 {
			ch2 <- num * num
		}
		// закрываем канал, неявно сигнализируя о завершение работы горутины
		close(ch2)
	}()
	// читаем до тех пор, пока канал ch2 не закрыт
	for num := range ch2 {
		fmt.Println(num)
	}
}
