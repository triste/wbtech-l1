package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// Lock-free счётчик реализованный с помощью атомарных операций
type CounterAtomic struct {
	val atomic.Int32
}

func (c *CounterAtomic) Increment() {
	c.val.Add(1)
}

func (c *CounterAtomic) Get() int32 {
	return c.val.Load()
}

// Счётчик реализованный с помощью мутекса
type CounterMutex struct {
	val int
	mu  sync.Mutex
}

func (c *CounterMutex) Increment() {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.val++
}

func (c *CounterMutex) Get() int {
	c.mu.Lock()
	defer c.mu.Unlock()
	return c.val
}

func main() {
	var (
		counterAtomic CounterAtomic
		counterMutex  CounterMutex
		wg            sync.WaitGroup
	)
	for range 1 << 10 {
		wg.Add(1)
		go func() {
			defer wg.Done()
			counterAtomic.Increment()
			counterMutex.Increment()
		}()
	}
	wg.Wait()
	fmt.Printf("atomic: %v, mutex: %v\n", counterAtomic.Get(), counterMutex.Get())
}
