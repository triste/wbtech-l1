package main

import "fmt"

func main() {
	nums := []int{1, 3, 5, 7, 123, 200}
	idx := bsearch(nums, 3)
	fmt.Println(idx)
	idx = bsearch(nums, 4)
	if idx == len(nums) {
		fmt.Println("значение 4 не найдено")
	}
}

func bsearch(nums []int, num int) int {
	left, right := 0, len(nums)-1
	for left < right {
		mid := (left + right) / 2
		if nums[mid] == num {
			return mid
		}
		if nums[mid] < num {
			left = mid + 1
		} else {
			right = mid
		}
	}
	return len(nums)
}
