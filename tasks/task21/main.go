package main

import "fmt"

type Repository interface {
	Insert(id int, val int) error
}

type DB struct {
}

func (d *DB) Exec(query string, args ...any) error {
	fmt.Println("data inserted")
	return nil
}

type DbAdapter struct {
	adaptee *DB
}

func (d *DbAdapter) Insert(id int, val int) error {
	fmt.Println("inserting data to database...")
	return d.adaptee.Exec("insert into sometbl values($1, $2)", id, val)
}

func main() {
	// Инициализируем сторонний сервис/библиотеку
	db := &DB{}
	// Оборачиваем в адаптер реализующий интерфейс клиента
	adapter := &DbAdapter{db}
	// Передаём клиенту интерфейс
	client(adapter)
}

func client(repo Repository) {
	repo.Insert(10, 100)
}
