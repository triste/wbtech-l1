package main

import (
	"fmt"
	"syscall"
	"time"
)

func main() {
	fmt.Println("Спим 2 секунды...")
	sleepChan(2)
	fmt.Println("Просыпаемся...")

	fmt.Println("Спим 2 секунды...")
	sleepSyscall(2)
	fmt.Println("Просыпаемся...")
}

func sleepSyscall(sec int64) {
	time := syscall.Timespec{
		Sec: sec,
	}
	syscall.Nanosleep(&time, nil)
}

func sleepChan(sec int) {
	<-time.After(time.Duration(sec) * time.Second)
}
