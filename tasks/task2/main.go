package main

import (
	"fmt"
	"sync"
)

func main() {
	nums := []int{2, 4, 6, 8, 10}
	var wg sync.WaitGroup
	for _, num := range nums {
		// Перед запуском горутины увеличиваем количество ожидаемых горутин
		wg.Add(1)
		// В 1.22 можно не передавать переменную num по значению, так как каждая
		// итерация создаёт новую переменную
		go func(num int) {
			// Перед завершением горутины уменьшаем количество ожидаемых горутин
			defer wg.Done()
			fmt.Println(num * num)
		}(num)
	}
	// Ждём завершения всех горутин
	wg.Wait()
}
