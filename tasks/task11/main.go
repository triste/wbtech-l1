package main

import "fmt"

func main() {
	nums1 := []int{2, 4, 6, 8, 10, 12, 14, 16}
	nums2 := []int{1, 2, 3, 4, 5, 8, 11, 16}
	// Самый быстрый способ найти пересчение с помощью мапы (O(n)), но требует
	// дополнительной памяти. Eсли это недопустимо, то сортируем их и
	// итерируемся сразу по двум (O(n*logn)).
	m := make(map[int]struct{}, len(nums1))
	for _, num := range nums1 {
		m[num] = struct{}{}
	}
	var intersection []int
	for _, num := range nums2 {
		if _, found := m[num]; found {
			intersection = append(intersection, num)
		}
	}
	fmt.Println(intersection)
}
