package main

import (
	"log"
	"math/rand"
	"sync"
)

type SyncMap[K comparable, V any] struct {
	// мутекс для синхронизации читателей и писателей в мапу, читатели могут параллельно читать
	mu sync.RWMutex
	m  map[K]V
}

func NewSyncMap[K comparable, V any]() *SyncMap[K, V] {
	return &SyncMap[K, V]{
		m: make(map[K]V),
	}
}

func (s *SyncMap[K, V]) Write(key K, val V) {
	// захватываем write лок, все остальные читатели и писатели будут ждать разблокировки
	s.mu.Lock()
	defer s.mu.Unlock()
	s.m[key] = val
}

func (s *SyncMap[K, V]) Read(key K) (V, bool) {
	// захватываем read лок, все писатели будет ждать разблокировки
	s.mu.RLock()
	defer s.mu.RUnlock()
	val, ok := s.m[key]
	return val, ok
}

func main() {
	var wg sync.WaitGroup
	m := NewSyncMap[int, int]()
	for i := 0; i < 1<<10; i++ {
		wg.Add(1)
		go func() {
			// Параллельно пишем в потокобезопасную мапу
			defer wg.Done()
			key := rand.Intn(4)
			val := rand.Intn(1000)
			m.Write(key, val)
			rval, _ := m.Read(key)
			if rval != val {
				log.Printf("Значение по ключу %v уже успело обновиться: %v -> %v", key, val, rval)
			}
		}()
	}
	wg.Wait()
}
