package main

import (
	"cmp"
	"fmt"
	"math/rand"
)

func main() {
	nums := []int{5, 1, 3, 123, 7, 1, 200}
	qsort(nums)
	fmt.Println(nums)
}

func qsort[T cmp.Ordered](nums []T) {
	if len(nums) < 2 {
		return
	}
	pivot := rand.Intn(len(nums))
	left, right := 0, len(nums)-1
	nums[right], nums[pivot] = nums[pivot], nums[right]
	for i, num := range nums {
		if num < nums[right] {
			nums[i], nums[left] = nums[left], nums[i]
			left++
		}
	}
	nums[left], nums[right] = nums[right], nums[left]
	qsort(nums[:left])
	qsort(nums[left+1:])
}
