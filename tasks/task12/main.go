package main

import "fmt"

func main() {
	words := []string{"cat", "cat", "dog", "cat", "tree"}
	// Изспользуем пустую структуру, так как нам не нужно значение и она имеет
	// размер 0 байт
	set := make(map[string]struct{})
	for _, word := range words {
		set[word] = struct{}{}
	}
	keys := make([]string, 0, len(set))
	for key := range set {
		keys = append(keys, key)
	}
	fmt.Println(keys)
}
